package ru.ekfedorov.tm.enumerated;

public enum Sort {

    NAME,
    CREATED,
    DATE_START,
    DATE_FINISH,
    STATUS

}
