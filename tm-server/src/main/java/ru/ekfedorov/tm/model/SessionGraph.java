package ru.ekfedorov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_session")
@JsonIgnoreProperties(ignoreUnknown = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SessionGraph extends AbstractGraphEntity implements Cloneable {

    @Column
    @Nullable
    private String signature;

    @Column
    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @Nullable
    @ManyToOne
    private UserGraph user;

    @Override
    public SessionGraph clone() {
        try {
            return (SessionGraph) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
