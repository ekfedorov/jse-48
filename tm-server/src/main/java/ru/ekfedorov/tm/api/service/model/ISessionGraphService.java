package ru.ekfedorov.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.model.SessionGraph;

public interface ISessionGraphService extends IGraphService<SessionGraph> {

    @Nullable SessionGraph close(@Nullable SessionGraph session);

    @Nullable
    SessionGraph open(String login, String password);

    @SneakyThrows
    void remove(@Nullable SessionGraph entity);

    void validate(@Nullable SessionGraph session);

    void validateAdmin(@Nullable SessionGraph session, @Nullable Role role);

}
