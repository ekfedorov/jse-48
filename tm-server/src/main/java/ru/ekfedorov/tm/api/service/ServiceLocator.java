package ru.ekfedorov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.service.dto.*;
import ru.ekfedorov.tm.api.service.model.*;

public interface ServiceLocator {

    @NotNull
    IProjectService getProjectDTOService();

    @NotNull
    IProjectGraphService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskDTOService();

    @NotNull
    IProjectTaskGraphService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionDTOService();

    @NotNull
    ISessionGraphService getSessionService();

    @NotNull
    ITaskService getTaskDTOService();

    @NotNull
    ITaskGraphService getTaskService();

    @NotNull
    IUserService getUserDTOService();

    @NotNull
    IUserGraphService getUserService();

    @NotNull
    IConnectionService getConnectionService();

}
