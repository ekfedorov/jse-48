package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public class UserIsLockedException extends AbstractException {

    public UserIsLockedException() {
        super("The user is locked...");
    }

}