package ru.ekfedorov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.api.service.model.IProjectGraphService;
import ru.ekfedorov.tm.api.service.model.ITaskGraphService;
import ru.ekfedorov.tm.api.service.model.IUserGraphService;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.marker.DBCategory;
import ru.ekfedorov.tm.model.ProjectGraph;
import ru.ekfedorov.tm.model.UserGraph;
import ru.ekfedorov.tm.service.TestUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProjectGraphServiceTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final IConnectionService connectionService = serviceLocator.getConnectionService();

    @NotNull
    private final IProjectGraphService projectService = serviceLocator.getProjectService();

    @NotNull
    private final IUserGraphService userService = serviceLocator.getUserService();

    @NotNull
    private final ITaskGraphService taskService = serviceLocator.getTaskService();

    {
        TestUtil.initUser();
    }

    @Before
    public void before() {
        connectionService.getEntityManager().getEntityManagerFactory().createEntityManager();
    }

    @After
    public void after() {
        connectionService.getEntityManager().getEntityManagerFactory().close();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<ProjectGraph> projects = new ArrayList<>();
        final ProjectGraph project1 = new ProjectGraph();
        final ProjectGraph project2 = new ProjectGraph();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertTrue(projectService.findOneById(project1.getId()).isPresent());
        Assert.assertTrue(projectService.findOneById(project2.getId()).isPresent());
        projectService.remove(projects.get(0));
        projectService.remove(projects.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final ProjectGraph project = new ProjectGraph();
        projectService.add(project);
        Assert.assertNotNull(projectService.findOneById(project.getId()));
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        taskService.clear();
        projectService.clear();
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void containsTest() {
        final ProjectGraph project = new ProjectGraph();
        final String projectId = project.getId();
        projectService.add(project);
        Assert.assertTrue(projectService.contains(projectId));
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int projectSize = projectService.findAll().size();
        final List<ProjectGraph> projects = new ArrayList<>();
        final ProjectGraph project1 = new ProjectGraph();
        final ProjectGraph project2 = new ProjectGraph();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertEquals(2 + projectSize, projectService.findAll().size());
        projectService.remove(project1);
        projectService.remove(project2);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllSortByUserId() {
        taskService.clear();
        projectService.clear();
        final List<ProjectGraph> projects = new ArrayList<>();
        final ProjectGraph project1 = new ProjectGraph();
        final ProjectGraph project2 = new ProjectGraph();
        final ProjectGraph project3 = new ProjectGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project1.setName("b");
        project2.setName("c");
        project3.setName("a");
        project1.setUser(user.get());
        project2.setUser(user.get());
        project3.setUser(user.get());
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        projectService.addAll(projects);
        final String sort = "NAME";
        final List<ProjectGraph> projects2 = new ArrayList<>(projectService.findAll(userId, sort));
        Assert.assertFalse(projects2.isEmpty());
        Assert.assertEquals(3, projects2.size());
        Assert.assertEquals("a", projects2.get(0).getName());
        Assert.assertEquals("b", projects2.get(1).getName());
        Assert.assertEquals("c", projects2.get(2).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final ProjectGraph project = new ProjectGraph();
        final String projectId = project.getId();
        projectService.add(project);
        Assert.assertNotNull(projectService.findOneById(projectId));
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final ProjectGraph project = new ProjectGraph();
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.findOneById(projectId).isPresent());
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTestByUserId() {
        final ProjectGraph project = new ProjectGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project.setUser(user.get());
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.findOneById(userId, projectId).isPresent());
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByNameTest() {
        final ProjectGraph project = new ProjectGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project.setUser(user.get());
        project.setName("pr1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(projectService.findOneByName(userId, name).isPresent());
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final ProjectGraph project = new ProjectGraph();
        projectService.add(project);
        final String projectId = project.getId();
        projectService.removeOneById(projectId);
        Assert.assertFalse(projectService.findOneById(projectId).isPresent());
    }


    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTestByUserId() {
        final ProjectGraph project = new ProjectGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project.setUser(user.get());
        projectService.add(project);
        final String projectId = project.getId();
        projectService.removeOneById(userId, projectId);
        Assert.assertFalse(projectService.findOneById(userId, projectId).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIndexTest() {
        final ProjectGraph project1 = new ProjectGraph();
        final ProjectGraph project2 = new ProjectGraph();
        final ProjectGraph project3 = new ProjectGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project1.setUser(user.get());
        project2.setUser(user.get());
        project3.setUser(user.get());
        projectService.add(project1);
        projectService.add(project2);
        projectService.add(project3);
        Assert.assertTrue(projectService.findOneByIndex(userId, 0).isPresent());
        Assert.assertTrue(projectService.findOneByIndex(userId, 1).isPresent());
        Assert.assertTrue(projectService.findOneByIndex(userId, 2).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByNameTest() {
        final ProjectGraph project = new ProjectGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project.setUser(user.get());
        project.setName("pr1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        projectService.removeOneByName(userId, name);
        Assert.assertFalse(projectService.findOneByName(userId, name).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final ProjectGraph project = new ProjectGraph();
        projectService.add(project);
        projectService.remove(project);
        Assert.assertNotNull(projectService.findOneById(project.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTestByUserIdAndObject() {
        final ProjectGraph project = new ProjectGraph();
        final @NotNull Optional<UserGraph> user = userService.findByLogin("test");
        final String userId = user.get().getId();
        project.setUser(user.get());
        projectService.add(project);
        projectService.remove(userId, project);
        Assert.assertFalse(projectService.findOneById(userId, project.getId()).isPresent());
    }

}
