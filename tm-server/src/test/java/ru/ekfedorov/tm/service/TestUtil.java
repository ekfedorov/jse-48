package ru.ekfedorov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.api.service.dto.IUserService;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.service.dto.UserService;

public class TestUtil {

    @NotNull
    static final IPropertyService propertyService = new PropertyService();

    @NotNull
    static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    static final IUserService userService = new UserService(propertyService, connectionService);

    public static void initUser() {
        if (!userService.findByLogin("test").isPresent()) {
            userService.create("test", "test", "test@test.ru");
        }
        if (!userService.findByLogin("test2").isPresent()) {
            userService.create("test2", "test", "test@test.ru");
        }
        if (!userService.findByLogin("admin").isPresent()) {
            userService.create("admin", "admin", Role.ADMIN);
        }
    }

}
